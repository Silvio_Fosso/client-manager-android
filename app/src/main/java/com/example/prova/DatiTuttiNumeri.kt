package com.example.prova

import com.google.gson.annotations.SerializedName

class DatiTuttiNumeri (

    @SerializedName("ministero_salute") val ministero_salute : Int,
    @SerializedName("unico_emergenza") val unico_emergenza : List<Int>,
    @SerializedName("numeri_regionali") val numeri_regionali : List<Numeri_regionali>
)
 class Numeri_regionali (

    @SerializedName("regione") val regione : String,
    @SerializedName("codice_regione") val codice_regione : Int,
    @SerializedName("numeri") val numeri : List<Int>
)