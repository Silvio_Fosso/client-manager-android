package com.example.prova

import RvAdapter2
import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class Numeri : AppCompatActivity() {

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val recyclerView = findViewById<RecyclerView>(R.id.Recyclevie)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        var lista = arrayListOf<datiCard4>()
        val sessionId = intent.getStringExtra("pos")
        okHttp1.shared.getAzienda(sessionId) {
            it.forEach {
                lista.add(datiCard4(it.id,it.data,it.dare,it.avere,it.descrizione))
            }


            runOnUiThread {
                val rvAdapter = RvAdapter2(lista)
               recyclerView.adapter = rvAdapter

                }
            }
        }
    }




