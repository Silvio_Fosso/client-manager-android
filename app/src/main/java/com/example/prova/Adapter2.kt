import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.get
import androidx.core.view.size
import androidx.recyclerview.widget.RecyclerView
import com.example.prova.*
//import com.example.prova.chartController
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import java.text.NumberFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class RvAdapter2(val userList: ArrayList<datiCard4>) : RecyclerView.Adapter<RvAdapter2.ViewHolder>() {


    override fun getItemViewType(position: Int): Int {

        return position
    }
    init {

    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
      lateinit var v : View
            v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_view2, p0, false)
        return ViewHolder(v);
    }
    override fun getItemCount(): Int {
        return userList.size
    }
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        var anno = userList[p1].Data.substring(0,4)
        var mese = userList[p1].Data.substring(5,7)+"-"
        var giorni = userList[p1].Data.substring(8,10)+"-"
        println(giorni)
        p0.data.text = "Data : "+giorni+mese+anno
        p0.dare.text = "Dare :"+userList[p1].dare.toString()+"€"
        p0.avere.text ="Avere : "+ userList[p1].avere.toString()+"€"
        p0.descr.maxLines = 2;
        p0.descr.text = "Descrizione : "+userList[p1].descrizione
    }








    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val data = itemView.findViewById<TextView>(R.id.giorno)
        val dare = itemView.findViewById<TextView>(R.id.dare)
        val avere = itemView.findViewById<TextView>(R.id.avere)
        val descr = itemView.findViewById<TextView>(R.id.descr)

    }




}