import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.get
import androidx.core.view.size
import androidx.recyclerview.widget.RecyclerView
import com.example.prova.R
import com.example.prova.clienti
//import com.example.prova.chartController
import com.example.prova.datiCard
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class RvAdapter(val userList: ArrayList<datiCard>) : RecyclerView.Adapter<RvAdapter.ViewHolder>(),Filterable {
 var copiaDati = ArrayList<datiCard>()
lateinit var mListener : OnItemClickListener

    interface OnItemClickListener{
        fun onItemClick(position : Int)
    }
    fun setOnItemClickListener(listener : OnItemClickListener){
       mListener = listener
    }
    override fun getItemViewType(position: Int): Int {

        return position
    }
    init {
        copiaDati = userList
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
      lateinit var v : View
            v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_view, p0, false)
        return ViewHolder(v,mListener);
    }
    override fun getItemCount(): Int {
        return copiaDati.size
    }
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

                var somma = 0f
                var differenza = 0f
                p0.tipodato.text = copiaDati[p1].uno
        userList[0].tre.forEach {
                    println(it.Codice)

                    if(it.Codice.toString() == copiaDati[p1].due){
                        println("entro")
                        println(it.avere)
                      somma+= it.avere
                        differenza+= it.dare
                    }

                }
               // p0.dare.text = "Devi dare\n"+differenza.toString()
               // p0.avere.text = "Devi ricevere\n"+somma.toString()
                p0.dati.text = "Devi avere : "+(somma-differenza).toString()+"€"






    }

    class ViewHolder(itemView: View,listener : OnItemClickListener) : RecyclerView.ViewHolder(itemView) {
        val tipodato = itemView.findViewById<TextView>(R.id.cliente)
        //val dare = itemView.findViewById<TextView>(R.id.textView4)
        //val avere = itemView.findViewById<TextView>(R.id.textView6)
        val dati = itemView.findViewById<TextView>(R.id.textView5)
        val line = itemView.findViewById<LineChart>(R.id.Line)
        val rotondo = itemView.findViewById<PieChart>(R.id.Circle)
        val bar = itemView.findViewById<BarChart>(R.id.Barra)
        val button = itemView.findViewById<Button>(R.id.angry_btn)

        init {

            button.setOnClickListener {
                val pos = adapterPosition
                 listener.onItemClick(pos)
            }
        }
        }


    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    copiaDati = userList
                } else {
                    val resultList = ArrayList<datiCard>()
                    for (row in copiaDati) {
                        if (row.uno!!.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    copiaDati = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = copiaDati
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                copiaDati = results?.values as ArrayList<datiCard>
                notifyDataSetChanged()
            }

        }
    }


}