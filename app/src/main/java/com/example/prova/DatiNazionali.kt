package com.example.prova

import com.google.gson.annotations.SerializedName


data class clienti (

   @SerializedName("Nome") val nome : String,
   @SerializedName("Codice") val codice : Int
)


data class aziende (

   @SerializedName("Id") val id : Int,
   @SerializedName("Data") val data : String,
   @SerializedName("Dare") val dare : Float,
   @SerializedName("Avere") val avere : Float,
   @SerializedName("Codice") val Codice : Int,
   @SerializedName("Descrizione") val descrizione : String
)
