package com.example.prova

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.*
import okhttp3.MultipartBody
import java.io.IOException
import java.lang.reflect.Type


class okHttp1 {
    val okHttpClient = OkHttpClient()

    fun getDatiClienti(callback: (Collection<clienti>) -> Unit){

        val request = Request.Builder()
            .url("https://pxgamers.altervista.org/Papa/FullAziende.php")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {

            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<clienti?>?>() {}.type
                val enums: Collection<clienti> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                println(enums);
                callback(enums)

            }
        })
    }


    fun getAziende(callback: (Collection<aziende>) -> Unit){

        val request = Request.Builder()
            .url("https://pxgamers.altervista.org/Papa/getevery.php")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {

            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<aziende?>?>() {}.type
                val enums: Collection<aziende> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                println(enums);
                callback(enums)

            }
        })
    }



    fun getAzienda(num : String,callback: (Collection<aziende>) -> Unit){
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("int", num)
            .build()
        val request = Request.Builder()
            .url("https://pxgamers.altervista.org/Papa/GetAzienda.php")
            .post(requestBody)
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {

            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<aziende?>?>() {}.type
                val enums: Collection<aziende> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                println(enums);
                callback(enums)

            }
        })
    }













    fun getDatiNumeriRegionali(callback: (DatiTuttiNumeri) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/andryg98/Covid-19/master/covid_numbers.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {


                val numeri = Gson().fromJson(response.body?.string(),DatiTuttiNumeri::class.java)

                callback(numeri)

            }
        })
    }




    companion object {
        val shared = okHttp1()
    }


}