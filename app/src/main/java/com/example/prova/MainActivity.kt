package com.example.prova

import RvAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler


class MainActivity : AppCompatActivity() {
//  var filteredMonthList : List<DatiProvincia> = it.filter { s -> s.codice_regione == 13 }
//        println(filteredMonthList)
    //PRENDERCI IL SINGOLO ELEMENTO


val dataList = ArrayList<datiCard>()
  lateinit  var client : Collection<clienti>
  lateinit  var dt : Collection<aziende>;
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<RecyclerView>(R.id.Recycle)
        val search = findViewById<SearchView>(R.id.Search)
        var mLinearLayoutManager =
            CarouselLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = mLinearLayoutManager;
        //recyclerView.layoutManager = layoutManager
        //recyclerView.layoutManager = layoutManager
        //recyclerView.setHasFixedSize(true)
       // recyclerView.adapter = TestAdapter(this)

//        pass the values to RvAdapter
        //val rvAdapter = RvAdapter(dataList)
//        set the recyclerView to the adapter

      //  recyclerView.adapter = rvAdapter;
        val cardWidthPixels: Float = this.resources.displayMetrics.widthPixels * 0.80f
        val cardHintPercent = 0.01f
       // recyclerView.addItemDecoration(RVPagerSnapFancyDecorator(applicationContext,  cardWidthPixels.toInt(), cardHintPercent))
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.app_bar)
        setSupportActionBar(toolbar)



            okHttp1.shared.getAziende {s ->

                okHttp1.shared.getDatiClienti {
                    client = it
                    it.forEach {
                        dataList.add(datiCard(it.nome, it.codice.toString(),s));
                    }
                    runOnUiThread {
                        val rvAdapter = RvAdapter(dataList)
                        recyclerView.adapter = rvAdapter
                        toolbar.isVisible = true
                        rvAdapter.setOnItemClickListener(object : RvAdapter.OnItemClickListener{
                            override fun onItemClick(position: Int) {
                              println(dataList[position].due)
                                val intent =
                                    Intent(baseContext, Numeri::class.java)
                                intent.putExtra("pos", dataList[position].due)
                                startActivity(intent)
                            }

                        })
                        search.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                            override fun onQueryTextSubmit(query: String?): Boolean {
                                return false
                            }

                            override fun onQueryTextChange(newText: String?): Boolean {
                                rvAdapter.filter.filter(newText)
                                return false
                            }

                        })
                    }
                }
            }





    }





}



class CarouselLayoutManager(
    context: Context?,
    orientation: Int,
    reverseLayout: Boolean
) :
    LinearLayoutManager(context, orientation, reverseLayout) {
    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: Recycler,
        state: RecyclerView.State
    ): Int {
        val orientation = orientation
        return if (orientation == HORIZONTAL) {
            val scrolled = super.scrollHorizontallyBy(dx, recycler, state)
            val minifyAmount = 0.25f
            val minifyDistance = 0.75f
            val parentMidpoint = width / 2f
            val d0 = 0f
            val d1 = parentMidpoint * minifyDistance
            val s0 = 1f
            val s1 = 1f - minifyAmount
            for (i in 0 until childCount) {
                val child = getChildAt(i)
                val childMidpoint =
                    (getDecoratedLeft(child!!) + getDecoratedRight(child)) / 2f
                val d =
                    Math.min(d1, Math.abs(parentMidpoint - childMidpoint))
                val scaleFactor = s0 + (s1 - s0) * (d - d0) / (d1 - d0)
                child.scaleX = scaleFactor
                child.scaleY = scaleFactor
            }
            scrolled
        } else {
            0
        }
    }

    override fun onLayoutChildren(
        recycler: Recycler,
        state: RecyclerView.State
    ) {
        super.onLayoutChildren(recycler, state)
        scrollHorizontallyBy(0, recycler, state)
    }
}